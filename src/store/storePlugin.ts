import Vue from 'vue';
import store from './StoreInstance';  // this is your store object

export default {
  store,
  // we can add objects to the Vue prototype in the install() hook:
  // tslint:disable-next-line:no-shadowed-variable
  install(Vue: any, options: any) {
    Vue.prototype.$globalStore = store;
  },
};
