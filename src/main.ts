import Vue from 'vue';
import App from './App.vue';
import router from './router';
import Snotify from 'vue-snotify';
import {SnotifyService} from 'vue-snotify/SnotifyService';
import storePlugin from './store/storePlugin';
import './bulmaImporter.scss';
import GlobalStore from '@/store/GlobalStore';

Vue.config.productionTip = false;
Vue.use(Snotify, {showProgressBar: false});
Vue.use(storePlugin);

declare module 'vue/types/vue' {
  interface Vue {
    $snotify: SnotifyService;
    $globalStore: GlobalStore;
  }
}

const app = new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
