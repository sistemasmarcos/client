import axios from 'axios';

const prefix = 'http://104.248.23.71:8090/api/gerenciamento/';

export const getAll = (table: string) => axios.get(`${prefix}${table}`);

export const insertItem = (table: string, data: any) => axios.post(`${prefix}${table}`, data);

export const updateItem = (table: string, data: any) => axios.patch(`${prefix}${table}`, data);

export const deleteItem = (table: string, data: any) => axios.delete(`${prefix}${table}/${data._id}`);
