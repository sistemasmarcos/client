import Form from './Form';
import FormField, { FormFieldParameters } from './FormField';
import ISelectValue from './ISelectValue';
import IColumns from '@/form-management/IColumns';

interface SelectFieldParameters {
    dbColumn: string;
    title: string;
    availableValues: ISelectValue[];
    value?: ISelectValue;
    relatedForm?: Form;
    columns: IColumns;
}

class SelectField extends FormField {
    public availableValues: ISelectValue[];
    public value: ISelectValue | undefined;
    constructor(params: SelectFieldParameters) {
        super({
            dbColumn: params.dbColumn,
            title: params.title,
            relatedForm: params.relatedForm,
            columns: params.columns,
        } as FormFieldParameters);
        if (params.availableValues === undefined) {
            this.availableValues = [];
        } else {
            this.availableValues = params.availableValues;
        }
        this.value = params.value;
        if (this.relatedForm !== undefined) {
            this.relatedForm.DataRefreshedCallback = () => {
                if (this.relatedForm === undefined) { return; }
                this.availableValues = [];
                for (const item of this.relatedForm.data) {
                    const displayValue = item[this.relatedForm.tableVisibleFields[0].dbColumn];
                    const value = item[this.relatedForm.tableVisibleFields[0].value];
                    this.availableValues.push({displayValue, value} as ISelectValue);
                }
            };
            this.relatedForm.refreshData();
        }
    }
}

export {SelectFieldParameters};
export default SelectField;
