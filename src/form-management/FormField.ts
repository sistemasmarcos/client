import Form from './Form';
import IColumns from './IColumns';

interface FormFieldParameters {
    dbColumn: string;
    title: string;
    relatedForm?: Form;
    columns: IColumns;
}

class FormField {
    public dbColumn: string;
    public title: string;
    public value: any;
    public defaultValue: any;
    public relatedForm: Form | undefined;
    public columns: IColumns;
    constructor(params: FormFieldParameters) {
        this.dbColumn = params.dbColumn;
        this.title = params.title;
        this.relatedForm = params.relatedForm;
        this.columns = params.columns;
    }
}

export {FormFieldParameters};
export default FormField;
