import axios from 'axios';
import FormField from './FormField';
import * as crud from '../api/commonCrud';

interface FormParameters {
    dbTable: string;
    singleTitle: string;
    pluralTitle: string;
    fields: FormField[];
    tableVisibleFields: FormField[];
    data: any[];
}

class Form {
    public dbTable: string;
    public singleTitle: string;
    public pluralTitle: string;
    public fields: FormField[];
    public tableVisibleFields: FormField[];
    public data: any[]; // dados atuais
    public editingId: string;
    public deletingIds: string[];
    public errored: boolean;
    public loading: boolean;
    public DataRefreshedCallback!: () => void;
    constructor(params: FormParameters) {
        this.dbTable = params.dbTable;
        this.singleTitle = params.singleTitle;
        this.pluralTitle = params.pluralTitle;
        this.fields = params.fields;
        this.tableVisibleFields = params.tableVisibleFields;
        this.data = [];
        this.editingId = '';
        this.deletingIds = [];
        this.errored = false;
        this.loading = false;
    }
    public resetFields() {
        for (const field of this.fields) {
            field.value = field.defaultValue;
        }
        this.editingId = '';
    }
    public refreshData() {
        this.loading = true;
        return crud.getAll(this.dbTable)

            .then((r) => {
                this.data = r.data;
                if (this.DataRefreshedCallback) {
                    this.DataRefreshedCallback();
                }
            })
            .catch((e) => {
                window.console.log(e);
            })
            .finally(() => {
                this.errored = false;
                this.loading = false;
                this.clearDeletingIds();
            });
    }
    public clearDeletingIds() {
        this.deletingIds = this.deletingIds.filter((x) => this.data.some((y) => y._id === x));
    }
    public resetValues() {
        for (const field of this.fields) {
            field.value = field.defaultValue;
        }
    }
}

export {FormParameters};
export default Form;
