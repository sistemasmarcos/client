import Form from './Form';
import FormField from './FormField';
import IColumns from '@/form-management/IColumns';

interface CheckFieldParameters {
    dbColumn: string;
    title: string;
    value: boolean;
    columns: IColumns;
    relatedForm?: Form;
}

class CheckField extends FormField {
    public value: boolean;
    constructor(params: CheckFieldParameters) {
        super({
            dbColumn: params.dbColumn,
            title: params.title,
            columns: params.columns,
            relatedForm: params.relatedForm,
        });
        this.value = params.value;
    }
}

export {CheckFieldParameters};
export default CheckField;
