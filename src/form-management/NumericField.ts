import Form from './Form';
import FormField, { FormFieldParameters } from './FormField';
import IColumns from '@/form-management/IColumns';

interface NumericFieldParameters {
    dbColumn: string;
    title: string;
    value: number;
    relatedForm?: Form;
    columns: IColumns;
}

class NumericField extends FormField {
    public value: number;
    constructor(params: NumericFieldParameters) {
        super({
            dbColumn: params.dbColumn,
            title: params.title,
            relatedForm: params.relatedForm,
            columns: params.columns,
        } as FormFieldParameters);
        this.value = params.value;
    }
}

export {NumericFieldParameters};
export default NumericField;
