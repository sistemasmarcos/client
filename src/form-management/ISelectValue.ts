interface ISelectValue {
  value: any;
  displayValue: string;
}

export default ISelectValue;
