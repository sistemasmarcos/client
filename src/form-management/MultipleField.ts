import Form from './Form';
import FormField, { FormFieldParameters } from './FormField';
import IColumns from '@/form-management/IColumns';

interface MultipleFieldParameters {
    dbColumn: string;
    title: string;
    availableValues: any[];
    values: any[];
    relatedForm?: Form;
    columns: IColumns;
}

class MultipleField extends FormField {
    public availableValues: any[];
    public values: any[];
    constructor(params: MultipleFieldParameters) {
        super({
            dbColumn: params.dbColumn,
            title: params.title,
            relatedForm: params.relatedForm,
            columns: params.columns,
        } as FormFieldParameters);
        this.availableValues = params.availableValues;
        this.values = params.values;
    }
}

export {MultipleFieldParameters};
export default MultipleField;
