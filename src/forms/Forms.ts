import {
    Form,
    TextField,
    CheckField,
    NumericField,
    SelectField,
    ISelectValue,
} from '../form-management/FormManagement';
import { FormParameters } from '@/form-management/Form';
import { TextFieldParameters } from '@/form-management/TextField';
import IColumns from '@/form-management/IColumns';

// common
export const createTextField = (dbColumn: string, title: string,
                                columns: IColumns = {mobile: 12, tablet: 12, computer: 12},
                                relatedForm?: Form) =>
    new TextField({
        dbColumn, title, columns, relatedForm,
    } as TextFieldParameters);

export const createSelectField = (dbColumn: string, title: string,
                                  columns: IColumns = {mobile: 12, tablet: 12, computer: 12},
                                  availableValues: ISelectValue[], relatedForm?: Form) =>
    new SelectField({
        dbColumn, title, columns, availableValues, relatedForm,
    });
export const createCheckField = (dbColumn: string, title: string,
                                 columns: IColumns = {mobile: 12, tablet: 12, computer: 12},
                                 value: boolean = false, relatedForm?: Form) =>
    new CheckField({
        dbColumn, title, value, columns, relatedForm,
    });
export const createNumericField = (dbColumn: string, title: string, value: number,
                                   columns: IColumns = {mobile: 12, tablet: 12, computer: 12},
                                   relatedForm?: Form) =>
    new NumericField({
        dbColumn, title, columns, value, relatedForm,
    });

export const createDescricaoField = (columns: IColumns = {mobile: 12, tablet: 12, computer: 12}) => new TextField({
    dbColumn: 'descricao',
    title: 'Descrição',
    columns,
} as TextFieldParameters);
export const createNomeField = (columns: IColumns = {mobile: 12, tablet: 12, computer: 12}) => new TextField({
    dbColumn: 'nome',
    title: 'Nome',
    columns,
} as TextFieldParameters);
