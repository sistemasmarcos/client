import Form, { FormParameters } from '@/form-management/Form';
import { createTextField, createCheckField, createNumericField } from '@/forms/Forms';
import SelectField, { SelectFieldParameters } from '@/form-management/SelectField';
import Cnaes from '@/forms/Cnaes';

const Empresas = new Form({
  dbTable: 'empresas',
  singleTitle: 'Empresa',
  pluralTitle: 'Empresas',
  fields: [
    createTextField('razaoSocial', 'Razão Social', {mobile: 12, tablet: 12, computer: 6}),
    createTextField('nomeFantasia', 'Nome Fantasia', {mobile: 12, tablet: 12, computer: 6}),
    new SelectField({
      dbColumn: 'cnae',
      title: 'CNAE',
      relatedForm: Cnaes,
      columns: {mobile: 12, tablet: 12, computer: 5},
    } as SelectFieldParameters),
    createTextField('cep', 'CEP', {mobile: 12, tablet: 12, computer: 3}),
    createTextField('estado', 'Estado', {mobile: 12, tablet: 12, computer: 4}),
    createTextField('cidade', 'Cidade', {mobile: 12, tablet: 12, computer: 6}),
    createTextField('bairro', 'Bairro', {mobile: 12, tablet: 12, computer: 6}),
    createTextField('endereco', 'Endereço', {mobile: 12, tablet: 12, computer: 9}),
    createNumericField('numero', 'Nº', 0, {mobile: 12, tablet: 12, computer: 3}),
    createTextField('telefone1', 'Telefone 1'),
    createTextField('telefone2', 'Telefone 2'),
    createTextField('contato1', 'Contato 1'),
    createTextField('contato2', 'Contato 2'),
    createTextField('data', 'Data'),
    createTextField('dataContrato', 'Data Contrato'),
    createTextField('dataVencimento', 'Data Vencimento'),
  ],
  tableVisibleFields: [
    createTextField('razaoSocial', 'Razão Social'),
    createTextField('imu', 'IMU'),
  ],
} as FormParameters);

export default Empresas;
