import Form, { FormParameters } from '@/form-management/Form';
import { createDescricaoField } from '@/forms/Forms';

const Cargos = new Form({
  dbTable: 'cargos',
  singleTitle: 'Cargo',
  pluralTitle: 'Cargos',
  fields: [
    createDescricaoField(),
  ],
  tableVisibleFields: [
    createDescricaoField(),
  ],
} as FormParameters);

export default Cargos;
