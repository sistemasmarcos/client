import Form, { FormParameters } from '@/form-management/Form';
import { createDescricaoField } from '@/forms/Forms';

const Setores = new Form({
  dbTable: 'setores',
  singleTitle: 'Setor',
  pluralTitle: 'Setores',
  fields: [
    createDescricaoField(),
  ],
  tableVisibleFields: [
    createDescricaoField(),
  ],
} as FormParameters);

export default Setores;
